"use client"

import Image from "next/image"
import Link from "next/link"

export default function Nav() {
    return (
        <div className="absolute top-0 left-0 w-full">
            <div className="main-container py-[2.6rem]">
                <div className="flex items-center justify-between">
                    <div className="relative h-[5.2rem] w-[5.2rem]">
                        <Image src={"/assets/logos/Apple-logo 1.svg"} fill alt={"logo apple"} />
                    </div>
                    <div className="flex items-center gap-[6.1rem]">
                        <Link href={"#"}>
                            <span className="text-[1.6rem] leading-[2.6rem] font-[300] text-[#8B8B8B]">{"Products"}</span>
                        </Link>
                        <Link href={"#"}>
                            <span className="text-[1.6rem] leading-[2.6rem] font-[300] text-[#8B8B8B]">{"Prices"}</span>
                        </Link>
                        <Link href={"#"}>
                            <span className="text-[1.6rem] leading-[2.6rem] font-[300] text-[#8B8B8B]">{"Overview"}</span>
                        </Link>
                        <Link href={"#"}>
                            <span className="text-[1.6rem] leading-[2.6rem] font-[300] text-[#8B8B8B]">{"Contacts"}</span>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}