"use client"

import Image from "next/image"

export default function Hero() {
    return (
        <div className="h-screen bg-gradient-to-r from-[#BABABA] flex items-center justify-start overflow-hidden relative">
            <div className="w-[700px] relative">
                <h2 className="text-[20rem] leading-[28rem] font-[800] text-white">{"Vision Pro"}</h2>
                <div className="absolute top-[70%] left-1/2 -translate-x-1/2 -translate-y-1/2 h-[64rem] w-[82.6rem]">
                    <Image src={"/assets/illustrations/apple-vision-pro 1.svg"} fill alt={"logo apple"} />
                </div>
            </div>
            <div className="w-[37rem]">
                <div className="relative h-[6rem] w-[6rem]">
                    <Image src={"/assets/logos/Apple-logo 2.svg"} fill alt={"logo apple"} />
                </div>
                <h3 className="text-[7rem] leading-[8rem] font-[800] text-[#474747] max-w-[32rem]">{"Vision Pro"}</h3>
                <div className="pt-[3rem] pb-[4rem]">
                    <p className="text-[1.6rem] leading-[2.6rem] font-[300] text-[#6F6F6F]">{"“Apple Vision Pro is the result of decades of experience designing high‑performance, mobile, and wearable devices “culminating in the most ambitious product Apple has ever created. Vision Pro integrates incredibly advanced technology into an elegant, compact form, resulting in an amazing experience every time you put it on.”"}</p>
                </div>
                <button
                    type="button"
                    className="h-[8rem] px-[9rem] bg-white rounded-full" 
                    style={{ boxShadow: "inset 6px 6px 14px #687389, inset -6px -6px 14px #ffffff" }}
                >
                    <span className="text-[2rem] font-[300] text-[#787878]">{"Read More"}</span>
                </button>
            </div>
            <div className="absolute -bottom-4 -right-4 h-[40rem] w-[38rem]">
                <Image src={"/assets/illustrations/apple-vision-pro 2.svg"} fill alt={"illustration apple vision pro"} />
            </div>
        </div>
    )
}