"use client"

import Image from "next/image"

export default function Innov() {
    return (
        <div className="bg-[#F6F5F8]">
            <div className="main-container pt-[24rem] pb-[10rem] relative ">
                <div className="max-w-[60rem]">
                    <h3 className="text-[6rem] leading-[6rem] text-[#AEAEAE] font-[800]">{"Innovation you can see, hear, and feel"}</h3>
                    <div className="max-w-[58.4rem] mt-[6rem] mb-[5rem]">
                        <p className="text-[1.6rem] text-[#6F6F6F] font-[400]">{"Pushing boundaries from the inside out."}</p>
                        <p className="text-[1.6rem] text-[#6F6F6F] font-[300]">{"Spatial experiences on Vision Pro are only possible through groundbreaking Apple technology. Displays the size of a postage stamp that deliver more pixels than a 4K TV to each eye. Incredible advances in Spatial Audio. A revolutionary dual‑chip design featuring custom Apple silicon. A sophisticated array of cameras and sensors. All the elements work together to create an unprecedented experience you have to see to believe."}</p>
                    </div>
                    <button
                        type="button"
                        className="h-[8rem] w-[8rem] bg-white rounded-full flex items-center justify-center" 
                        style={{ boxShadow: "inset 6px 6px 14px #687389, inset -6px -6px 14px #ffffff" }}
                    >
                        <svg width="40" height="24" viewBox="0 0 89 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 11.7556H88M88 11.7556L79.64 1M88 11.7556L79.64 23" stroke="#818181"/>
                        </svg>
                    </button>
                </div>
                <div className="absolute bottom-0 right-0 h-[89rem] w-[60rem]">
                    <Image src={"/assets/illustrations/apple_vision_pro-sixteen_nine 1.svg"} fill alt={"illustration apple vision pro"} />
                </div>
                <div className="absolute bottom-[-36rem] right-0 h-[64rem] w-[76rem] z-50">
                    <Image src={"/assets/illustrations/apple-vision-pro 3.svg"} fill alt={"illustration apple vision pro"} />
                </div>
            </div>
        </div>
    )
}