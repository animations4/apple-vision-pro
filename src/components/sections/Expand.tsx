"use client"

import Image from "next/image"

export default function Expand() {
    return (
        <div className="main-container pt-[24rem] pb-[20rem] relative bg-gradient-to-r from-[#BABABA] overflow-hidden">
            <div className="flex items-center justify-end">
                <div className="max-w-[60rem]">
                    <h3 className="text-[6rem] leading-[6rem] text-[#474747] font-[800]">{"Expand your surroundings"}</h3>
                    <div className="max-w-[40rem] my-[6rem]">
                        <p className="text-[1.6rem] text-[#6F6F6F] font-[300]">{"Environments let you transform the space around you, so apps can extend beyond the dimensions of your room. Choose from a selection of beautiful landscapes, or magically replace your ceiling with a clear, open sky. The Digital Crown gives you full control over how immersed you are."}</p>
                    </div>
                    <button
                        type="button"
                        className="h-[8rem] px-[9rem] bg-white rounded-full" 
                        style={{ boxShadow: "inset 6px 6px 14px #687389, inset -6px -6px 14px #ffffff" }}
                    >
                        <span className="text-[2rem] font-[300] text-[#787878]">{"Read More"}</span>
                    </button>
                </div>
            </div>
            <div className="absolute bottom-[-11rem] left-0 h-[84rem] w-[74rem]">
                <Image src={"/assets/illustrations/apple-vision-pro 4.svg"} fill alt={"illustration apple vision pro"} />
            </div>
        </div>
    )
}