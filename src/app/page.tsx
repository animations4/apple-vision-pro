"use client"

import Nav from "@/components/layouts/Nav";
import Expand from "@/components/sections/Expand";
import Hero from "@/components/sections/Hero";
import Innov from "@/components/sections/Innov";
import { LocomotiveScrollProvider } from "react-locomotive-scroll";
import { useRef } from "react";

export default function Home() {

  const ref = useRef(null);

  const options = {
    smooth: true,
  } 


  return (
    <LocomotiveScrollProvider options={options} containerRef={ref}>
      <div data-scroll-container ref={ref}>
        <Nav />
        <Hero />
        <Innov />
        <Expand />
      </div>
    </LocomotiveScrollProvider>
  );
}
